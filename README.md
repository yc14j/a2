> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Yarin Cohen

### Assignment # 2 Requirements:
1. Android studio healthy recipe application.

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;



#### Assignment Screenshots:

*Screenshot of running application's first user interface;*:

![First User Interface](imgs/Interfaceone.PNG)

*Screenshot of running application's second user interface;*:

![Second User Interface](imgs/Interfacetwo.PNG)